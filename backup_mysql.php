<?php
//Connection infos
$host = 'localhost';
$user = 'database_username';
$password = 'database_password';
$dbName = 'database_name';

//______________________________________________

header("Content-type: text; charset=utf-8");

header("Content-Disposition:attachment;filename=convert_at_" . time() . ".sql");

mysql_connect($host, $user, $password);

mysql_select_db($dbName);

if (mysql_errno ()) {

	die(mysql_error() . "\n");

}

$variablesRs = mysql_query('Show variables');

if (mysql_errno ()) {

	die(mysql_error() . "\n");

}

while ((list($variable, $value) = mysql_fetch_array($variablesRs)) !==

false) {

	if ($variable == 'max_allowed_packet') {

		$maxAllowedPacket = $value;

		break;

	}

}

echo "-- Dumping $dbName\n\n";

echo "SET NAMES utf8;\n\n";

//use this line bellow if you want to convert from iso to utf-8
//mysql_query('Set names latin1');

$tablesRs = mysql_query('Show table status');

if (mysql_errno ()) {

	die(mysql_error() . "\n");

}

while ((list($tableName, $engine, $version, $rowFormat, $rows,

$avgRowLength) = mysql_fetch_array($tablesRs)) !== false) {

	list($tableName, $tableCreate) = mysql_fetch_row(mysql_query("Show

create table `$tableName`"));

	$columns = 0;

	$tableDef = array();

	$tableDefRs = mysql_query("Describe `$tableName`");

	while (($columnDef = mysql_fetch_assoc($tableDefRs)) !== false) {

		$tableDef[] = $columnDef;

		$columns++;

	}

	echo "--\n-- Table structure for $tableName\n--\n\n";

	echo "$tableCreate;\n\n";

	echo "--\n-- Dumping data for $tableName\n--\n\n";

	echo "LOCK TABLES `$tableName` WRITE;\n";

	if ($engine == 'MyISAM') {

		echo "ALTER TABLE `$tableName` DISABLE KEYS;\n";

	}

	$start = 0;

	if ($avgRowLength > 0) {

		//echo "max packet = $maxAllowedPacket, avg length = $avgRowLength\n";

		$rowBatchLimit = (int) ($maxAllowedPacket / $avgRowLength);

	} else {

		$rowBatchLimit = 10000;

	}

	//echo "limit = $rowBatchLimit\n";

	$rowsRs = mysql_query("SELECT * FROM `$tableName` LIMIT $start,

	$rowBatchLimit");

	if (mysql_errno ()) {

		die(mysql_error() . "\n");

	}

	while (mysql_numrows($rowsRs)) {

		echo "INSERT INTO `$tableName` VALUES ";

		$numRows = 0;

		while (($row = mysql_fetch_row($rowsRs)) !== false) {

			if ($numRows != 0) {

				echo ",";

			}

			echo "(";

			for ($i = 0; $i < $columns; $i++) {

				if (strpos($tableDef[$i]['Type'], 'int') === false

				&& strpos($tableDef[$i]['Type'], 'float') === false

				&& strpos($tableDef[$i]['Type'], 'dec') === false

				&& strpos($tableDef[$i]['Type'], 'numeric') === false

				&& strpos($tableDef[$i]['Type'], 'real') === false

				&& strpos($tableDef[$i]['Type'], 'bool') === false

				&& strpos($tableDef[$i]['Type'], 'bit') === false

				&& strpos($tableDef[$i]['Type'], 'double') === false) {

					if ($row[$i] !== null || $tableDef[$i]['NULL'] == 'NO') {

						$row[$i] = "'" . 
						mysql_real_escape_string(mb_convert_encoding($row[$i], 'UTF-8', 'HTML-ENTITIES'))
						."'";

					} else {

						$row[$i] = "'null'";

					}

				}

				if ($row[$i] === null || $row[$i] === '') {

					if ($tableDef[$i]['NULL'] == 'NO') {

						$row[$i] = 0;

					} else {

						$row[$i] = "NULL";

					}

				}

				if ($i != 0) {

					echo ",";

				}

				echo $row[$i];

			}

			echo ")";

			$numRows++;

		}

		echo ";\n";

		$start += $rowBatchLimit;

		$rowsRs = mysql_query("SELECT * FROM `$tableName` LIMIT $start,

		$rowBatchLimit");

	}

	echo ";\n";

	if ($engine == 'MyISAM') {

		echo "ALTER TABLE `$tableName` ENABLE KEYS;\n";

	}

	echo "UNLOCK TABLES;\n\n";

}